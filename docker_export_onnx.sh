#!/bin/bash

./docker_run.sh python3 export.py --weights /workspace/yolov7.pt --grid --end2end --simplify --topk-all 100 --iou-thres 0.65 --conf-thres 0.35 --img-size 640 640

