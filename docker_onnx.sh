#!/bin/bash

for iou in 0.65 0.75 0.85 0.95
do
for thresh in 0.35 0.25 0.15 0.05
do

# removed --end2end 
#echo "yolov7_"${iou:2:3}"_"${thresh:2:3}".onnx"
./docker_run.sh python3 export.py --weights /workspace/yolov7-tiny.pt --grid --simplify --topk-all 100 --iou-thres $iou --conf-thres $thresh --img-size 640 640
mv yolov7-tiny.onnx onnx/"yolov7-tiny_"${iou:2:3}"_"${thresh:2:3}".onnx"


done
done
