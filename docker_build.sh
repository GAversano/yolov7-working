#!/bin/bash

if [ -f '/etc/nv_tegra_release' ]; then
    arch=l4t
    echo 'l4t'
else
    arch=x86
    echo 'x86'
fi

docker build --rm -t lblanp/yolov7-model-creator:latest -f Dockerfile.${arch} .
