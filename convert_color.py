import cv2
import argparse
import os 

def convert():
    source = opt.source

    for file in os.listdir(source):
       print(file)
       print(os.path.join("convert_image_colors", file))
       print(os.path.join(source, file))
       cv2.imwrite(os.path.join("convert_image_colors", file),\
           cv2.cvtColor(cv2.imread(os.path.join(source, file)), cv2.COLOR_BGR2RGB))



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--source', type=str, help='source')  # file/folder, 0 for webcam
   
    opt = parser.parse_args()

    convert()


