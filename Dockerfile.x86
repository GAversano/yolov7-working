# based on: https://docs.nvidia.com/deeplearning/tensorrt/container-release-notes/rel_20-09.html (yolov4-tiny-model-creator)
# and https://github.com/WongKinYiu/yolov7
FROM nvcr.io/nvidia/pytorch:21.08-py3

# setup environment
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    L4T_VER=32.4.4 \
    DEBIAN_FRONTEND=noninteractive

# https://forums.developer.nvidia.com/t/issues-building-docker-image-from-ngc-container-nvcr-io-nvidia-pytorch-22-py3/209034/4
ENV PATH="${PATH}:/opt/hpcx/ompi/bin"
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/opt/hpcx/ompi/lib"

# setup timezone, install deps, add the ROS deb repo, install bootstrap dependencies
RUN echo 'America/Los_Angeles' > /etc/timezone \
    && ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime \
    && apt-get update \
    && apt-get install -q -y --no-install-recommends \
    && apt-get install -y zip htop screen libgl1-mesa-glx \
        tzdata \
        git \
		cmake \
		build-essential \
		curl \
		wget \
		lsb-release \
        libpython3-dev \
        python3-pip \
        python3-opencv \
        python3-numpy \
        libopencv-dev \
        python3-h5py \
    && python3 -m pip install --no-cache-dir --upgrade pip \
    && python3 -m pip install --no-cache-dir setuptools wheel \
    && python3 -m pip install --no-cache-dir six pycuda \
    && python3 -m pip install --no-cache-dir matplotlib>=3.2.2 \
    && python3 -m pip install --no-cache-dir numpy>=1.18.5 \
    && python3 -m pip install --no-cache-dir opencv-python==4.5.5.64 \
    && python3 -m pip install --no-cache-dir Pillow>=7.1.2 \
    && python3 -m pip install --no-cache-dir PyYAML>=5.3.1 \
    && python3 -m pip install --no-cache-dir requests>=2.23.0 \
    && python3 -m pip install --no-cache-dir scipy>=1.4.1 \
    && python3 -m pip install --no-cache-dir torch>=1.7.0,!=1.12.0 \
    && python3 -m pip install --no-cache-dir torchvision>=0.8.1,!=0.13.0 \
    && python3 -m pip install --no-cache-dir tqdm>=4.41.0 \
    #/bin/bash: 4.21.3: No such file or directory
    #&& python3 -m pip install --no-cache-dir protobuf<4.21.3 \
    && python3 -m pip install --no-cache-dir tensorboard>=2.4.1 \
    && python3 -m pip install --no-cache-dir pandas>=1.1.4 \
    && python3 -m pip install --no-cache-dir seaborn>=0.11.0 \
    && python3 -m pip install --no-cache-dir ipython \
    && python3 -m pip install --no-cache-dir psutil \
    && python3 -m pip install --no-cache-dir thop \
    && rm -rf /var/lib/apt/lists/*

ENV WORKSPACE=/workspace \
    YOLOV7_LIBS_PATH=/workspace
WORKDIR /workspace

COPY . .

CMD ["bash"]

