# yolov7-working

Working repo to run yolov7 based on `official repo`https://github.com/WongKinYiu/yolov7.

Used primarily for training. Once an updated weight file is obtained, convert to onnx then use the `yolov7-model-creator repo`https://gitlab.com/lbl-anp/tracking/yolov7-model-creator.


Use at your own risk :) 
