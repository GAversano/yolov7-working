#!/bin/bash

# Note: any command line argument past to this script will be used as the docker run command.

nvidia-docker run --rm -v "$(pwd)":/workspace -v /data/:/coco/ -v "$(pwd)"/:/yolov7 -v "$(pwd)":/data -it --shm-size=64g lblanp/yolov7-model-creator:latest $@


#docker run --runtime nvidia --gpus all -it -v "$(pwd)":/workspace lblanp/yolov7-model-creator:latest $@
